==================
Cirrascale Sandbox
==================

This document will show you how to get up and running with the Cirrascale Sandbox.

Quick Start
-----------

All GPU servers in our Sandbox are managed by a Kubernetes API server. Users interact with the Sandbox using a Kubernetes API client called `kubectl` from their local systems. The connection between `kubectl` and the API server is further protected by an SSH tunnel. To summarize::

	User -> kubectl --{SSH tunnel}--> API Server -> GPU Servers

If you haven't already, please contact Cirrascale as instructed in your Introduction Letter to get started in the Sandbox.

1) Install kubectl
~~~~~~~~~~~~~~~~~~

The Kubernetes API client `kubectl` will be used to interact with the cluster. This tool will allow you to not only launch and monitor your pods but also copy data to and from the environment. Kubectl is cross-platform and instructions for acquiring it for your platform can be found here. 

* Linux::

	curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.8.13/bin/linux/amd64/kubectl
	chmod +x ./kubectl
	sudo mv ./kubectl /usr/local/bin/kubectl

* macOS::

	curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.8.13/bin/darwin/amd64/kubectl
	chmod +x ./kubectl
	sudo mv ./kubectl /usr/local/bin/kubectl

* Windows::

	curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.8.13/bin/windows/amd64/kubectl.exe

Please find more detailed instructions here if needed.
https://kubernetes.io/docs/tasks/tools/install-kubectl/

2) Save kubeconfig
~~~~~~~~~~~~~~~~~~

You should have received a kubeconfig file from Cirrascale. It is recommended and most convenient to save it as a file at this location ``$HOME/.kube/config``. Note that "config" is a file name, not a directory. ::

	# for example:
	mkdir $HOME/.kube
	cp <your org>-<your name>.kubeconfig $HOME/.kube/config

The Kubernetes API client `kubectl` references a configuration file known as `kubeconfig` when accessing the Kubernetes API. The `kubeconfig` is mostly a convenience that contains API specific keys, preferences, and addresses to save the user from entering that information over and over. When saved in the default location for your platform you won't need to tell `kubectl` where to look for it.

**Note:** The kubeconfigs we issue do expire. If you encounter an error such as ``You must be logged in to the server (the server has asked for the client to provide credentials)`` you may need to contact Cirrascale to renew the kubeconfig.

3) SSH Tunnel
~~~~~~~~~~~~~

We provide secure access to the Sandbox environment using SSH keys. If you do not already have keys, you may follow generic instructions to generate a key pair for yourself. Retain and protect the private key but send Cirrascale your public key.

Cirrascale will respond with a user name to use in combination with your private key to connect to our SSH tunnel gateway::

	ssh -i <your private key> -L 6443:172.19.0.100:6443 -N <user name>@yc.cirrascale.com

Once connected you will be able to talk to the API server running in our cloud at the private non-routing IP 172.19.0.100 via `localhost` or `127.0.0.1` on your system. Your kubeconfig has already been configured for this.

**Note:** Our gateway will display a banner informing you that it does not support any interactive shells. This is expected but it will block your terminal. To continue you may like to either 1) open a new terminal 2) background the ssh tunnel by appending a ``&`` to the end of the ``ssh`` command or 3) background it by supplying the ``-f`` flag to the ``ssh`` command. Whatever works best for your workflow.


4) Launch a Pod
~~~~~~~~~~~~~~~

Get started by launching a trivial pod. From your system, after kubectl is installed and the SSH tunnel is established, try running:

.. code-block:: sh

	cat <<EOF | kubectl apply -f -
	kind: Pod
	apiVersion: v1
	metadata:
	  name: hello-sandbox
	spec:
	  containers:
	  - name: busybox
	    image: busybox
	    command: ["sleep", "99999"]
	EOF

5) Interact with a Pod
~~~~~~~~~~~~~~~~~~~~~~

If all went well you now have a pod running in the Sandbox. Verify by running::

	kubectl get pods

If something went wrong or you're curious about how your pod is deployed::

	kubectl describe pod hello-sandbox

You can run processes in the pod using ``kubectl exec`` including interactive processes like a shell::

	kubectl exec -ti hello-sandbox sh

Finally, when you lose interest in this trivial busybox pod because you are ready to start using some of our GPU resources, delete the pod::

	kubectl delete pod hello-sandbox

Basic Operations
----------------

List nodes, with extra information (note: currently unavailable, sorry!)::

	kubectl get nodes -o wide

List pods::

	kubectl get pods

Start a pod::

	kubectl apply -f <path to pod YAML>

Stop/delete a pod::

	kubectl delete pod <name of pod>

Use a pod/container interactively:

1. Start a pod with a long running process (ie: sleep "forever")
2. Use `kubectl` to connect to a shell running in the pod::

	kubectl exec <name of pod> -ti bash

Get system information about a pod::

	kubectl describe pod <name of pod>

Display any available output (STDERR/STDOUT) produced by a pod::

	kubectl logs <name of pod>

Data Transfer
-------------

Use `kubectl cp` to copy data to and from your Sandbox storage.

1. Ensure that you are running a pod that has a mounted NAS share and will run long enough to receive data.
2. Use `kubectl` to copy data to the NAS share via your pod.::

	# upload data
	kubectl cp <path to your directory or file> <name of pod>:<remote path>

	# download data
	kubectl cp <name of pod>:<remote path> <path to your directory or file>

Private Repositories
--------------------

You may access your own private repositories by loading a Kubernetes secret object.::

	kubectl create secret docker-registry <name of secret> --docker-server=<your registry server> --docker-username=<your name> --docker-password=<your pword> --docker-email=<your email>

The resulting `secret` is then accessible by your pod defition via the `imagePullSecrets` section:

.. code-block:: yaml

	apiVersion: v1
	kind: Pod
	metadata:
	  name: ...
	spec:
	  containers:
	    - name: ...
	      image: <your private image>
	  imagePullSecrets:
	    - name: <name of secret>

Common Training Data
--------------------

Common training data such as CIFAR, COCO, Imagenet, and others may be accessed via a special read-only persistent volume called "data". To include access to this data in your pod add "volume" and "volumeMounts" sections to your YAML.

In this snippet the "data" is mounted in the container at the path "/data".

.. code-block:: yaml

	kind: Pod
	apiVersion: v1
	metadata:
	  name: ...
	spec:
	  containers:
	  - name: ...
	    image: ...
	    volumeMounts:
	    - name: data
	      mountPath: /data
	      readOnly: true
	  volumes:
	  - name: data
	    persistentVolumeClaim:
	      claimName: data

Pod Definition
--------------

As is everything in Kubernetes, pods are defined by a YAML representation. A pod may include multiple containers but its simplest to begin with just a single container per pod.

When deploying a pod of your own, adjust the YAML file to:

1. Run a container of your choice by editing the "image" field
2. Run a specific command by editing the "command" field
3. Adjust the number of GPUs from 0 to 8

This is just a sample of what you can do. There are of course many more capabilities to explore.

Examples
--------

This is the simplest pod YAML that will use 1 GPU, in this case for running TensorFlow:

.. code-block:: yaml

  kind: Pod
  apiVersion: v1
  metadata:
    name: basic-tensorflow
  spec:
    containers:
    - name: tensorflow
      image: tensorflow/tensorflow:latest-gpu
      command: ["sleep", "99999"]
      resources:
        limits:
          alpha.kubernetes.io/nvidia-gpu: 1
      volumeMounts:
      - name: nvidia-driver
        mountPath: /usr/local/nvidia
        readOnly: true
    volumes:
    - name: nvidia-driver
      persistentVolumeClaim:
        claimName: nvidia-driver

Next, here is a pod YAML that launches the same pod above but also:

* attaches the /home directory to your read-write NAS space
* attaches the /data directory to the read-only common training data
* requests to use 4 GPUs

.. code-block:: yaml

  kind: Pod
  apiVersion: v1
  metadata:
    name: full-tensorflow
  spec:
    containers:
    - name: tensorflow
      image: tensorflow/tensorflow:latest-gpu
      command: ["sleep", "99999"]
      workingDir: /home
      resources:
        limits:
          alpha.kubernetes.io/nvidia-gpu: 4
      volumeMounts:
      - name: nvidia-driver
        mountPath: /usr/local/nvidia
        readOnly: true
      - name: nas
        mountPath: /home
      - name: data
        mountPath: /data
        readOnly: true
    volumes:
    - name: nvidia-driver
      persistentVolumeClaim:
        claimName: nvidia-driver
    - name: nas
      persistentVolumeClaim:
        claimName: nas
    - name: data
      persistentVolumeClaim:
        claimName: data

And here is an example that launches the same pod as above but specifically requests to be scheduled on a system with "Maxwell" generation GPUs via the "nodeSelector" section:

.. code-block:: yaml

  kind: Pod
  apiVersion: v1
  metadata:
    name: full-tensorflow
  spec:
    containers:
    - name: tensorflow
      image: tensorflow/tensorflow:latest-gpu
      command: ["sleep", "99999"]
      workingDir: /home
      resources:
        limits:
          alpha.kubernetes.io/nvidia-gpu: 4
      volumeMounts:
      - name: nvidia-driver
        mountPath: /usr/local/nvidia
        readOnly: true
      - name: nas
        mountPath: /home
      - name: data
        mountPath: /data
        readOnly: true
    volumes:
    - name: nvidia-driver
      persistentVolumeClaim:
        claimName: nvidia-driver
    - name: nas
      persistentVolumeClaim:
        claimName: nas
    - name: data
      persistentVolumeClaim:
        claimName: data
    nodeSelector:
      gpu_gen: maxwell

We currently support the following node selector fields:

* ``gpu_gen`` architecture generation

  * ``maxwell``
  
  * ``pascal``

* ``gpu_class`` GPU product class

  * ``geforce``

  * ``tesla``

* ``gpu_mem`` GPU memory in GB

  * ``11``

  * ``24``

* ``gpu_p2p`` maximum peer-to-peer GPUs

  * ``8``
